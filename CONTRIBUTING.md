# Contributing

When contributing to this repository, please first read this text document and fully understand it before continuing. This will be updated constantly throughout the development process, and act somewhat as a wiki for getting started.

## Building

When building for the first time, you must do the following process:
1. Download Unreal Engine **4.20.3** through [source].(https://docs.unrealengine.com/en-US/GettingStarted/DownloadingUnrealEngine/index.html) and build it.
2. Download Visual Studio 2017 (Community is sufficient). Visual Studio 2019 is not currently supported by Unreal's documentations (however in personal trials it does seem to work ok, but expect bugs/weird things).
3. (Optional) Setup Visual Studio to debug by getting it to work with Unreal Engine 4. Please see this [wiki](https://docs.unrealengine.com/en-US/Programming/Development/VisualStudioSetup/index.html) for a guide.
4. Once setup, in Unreal Engine do `File->Open Visual Studio`. This will both launch Visual Studio as well as create a `.sln` solution file.
5. In **Unreal Engine**, compile the C++ code. This is required before you can launch Unreal Engine 4 (and the project) as debug mode within Visual Studio 2017.
6. (Optional) You can now close Unreal Engine 4, and from now on launch Unreal Engine 4 in debug mode through the visual studio project by hitting F5 (or the `> Local Windows Debugger` button on the top of Visual Studio).

Building this route will dramatically improve the debugging experience of Unreal Engine software, and allow stepping in C++ code.

## GitLab

These categories below are dedicated to the GitLab process itself.

### Issues

Issues serves as GitLab's version of product backlog items (PBIs) or stories. These are categorized as bugs, impediments, and other deliverable and non-deliverable work items.

### Branches & Merging

Branches should be created for each issue, and issues should be merged with master through merge requests. Further down the line a build process might be introduced that requires the merged items produce a working build, and perhaps tests will be ran against it. If it fails, then merging will not work.

### Issue Labels

Work Flows (Optional; only tag when needed):
* Blocked 
* Doing
* To Do

Category of backlog Item (Each issue should be tagged with 1 of these):
* Debugging Tools - Non-user facing features added only for debugging purposes.
* Bug - Unexpected and/or undesired behavior that should be addressed ASAP, including performance problems.
* Feature - User facing features or behaviors, from business logic changes to UI. These are synonymous with regular stories or PBIs.
* Spike - Used to gather or produce more information regarding a topic. Usually used when researching technology is required for upcoming issues. This is synonymous with Discoveries in other agile practices. 
* TUS - No features are usually expected to be added outside of infrastructure. This is expected for not only research (like a spike), but may include creating a deliverable such as a plan for further development.

### Tags
Git tags should start at `0.0.0.0` and use regular semantic versioning. Upon inital release, we will start it at `1.0.0.0`. Once testing begins, we will use both `-alpha` and `-beta` tags appended to the version.

### Packages
No packages exist yet. Coming soon.

### Wiki
No wiki exists yet. Coming soon.

### Settings
Settings can only be changes by the administrators of the repository. If there's a setting in GitLab you'd like changed by us, please either notify us in our Slack channel or contact one of the higher-ranking authors.