// Copyright 2018 DevNull LLC, All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class TDTarget : TargetRules
{
	public TDTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "TD" } );
	}
}