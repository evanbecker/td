// Copyright 2018 DevNull LLC, All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class TDEditorTarget : TargetRules
{
	public TDEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "TD" } );
	}
}