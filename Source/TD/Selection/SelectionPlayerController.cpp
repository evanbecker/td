// Copyright 2018 DevNull LLC, All Rights Reserved.

#include "SelectionPlayerController.h"

ASelectionPlayerController::ASelectionPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}