// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SelectionPlayerController.generated.h"

UCLASS()
class TD_API ASelectionPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ASelectionPlayerController();
};