// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

UENUM(BlueprintType)
enum class SelectEnum : uint8
{
	SelOn,
	SelOff,
	SelHighlight
};