// Copyright 2018 DevNull LLC, All Rights Reserved.

#include "SelectionPawn.h"
#include "SelectionPlayerController.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Grid/Cell.h"
#include "Towers/Tower.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "Engine/GameViewportClient.h"

ASelectionPawn::ASelectionPawn(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer), CurrentCellFocus(nullptr)
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	OurCameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	OurCameraSpringArm->SetupAttachment(RootComponent);
	OurCameraSpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.0f), FRotator(-60.0f, 0.0f, 0.0f));
	OurCameraSpringArm->TargetArmLength = 0.f;
	OurCameraSpringArm->bEnableCameraLag = true;
	OurCameraSpringArm->CameraLagSpeed = 3.0f;

	OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
	OurCamera->SetupAttachment(OurCameraSpringArm, USpringArmComponent::SocketName);

	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

void ASelectionPawn::BeginPlay()
{
	Super::BeginPlay();
}


void ASelectionPawn::CalculateCameraTransform(const float mouse_x, const float mouse_y, const FVector2D viewport_size)
{
	auto current_location = this->GetActorLocation();
	if (mouse_x > 0.9f && mouse_x < EdgeLeftRightSize)
	{
		current_location.Y -= Speed;
	}
	if (mouse_x > viewport_size.X - EdgeLeftRightSize)
	{
		current_location.Y += Speed;
	}
	if (mouse_x > 0.9f && mouse_y < EdgeTopBottomSize)
	{
		current_location.X += Speed;
	}
	if (mouse_y > viewport_size.Y - EdgeTopBottomSize)
	{
		current_location.X -= Speed;
	}
	SetActorLocation(current_location);
}

void ASelectionPawn::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
	float mouse_x, mouse_y;
	if (const auto pc = Cast<APlayerController>(GetController()))
	{
		if (GEngineAndViewportExists())
		{
			pc->GetMousePosition(mouse_x, mouse_y);
			const auto viewport_size = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
			CalculateCameraTransform(mouse_x, mouse_y, viewport_size);

			FVector start, dir;
			pc->DeprojectMousePositionToWorld(start, dir);
			const auto end = start + (dir * 8000.0f);
			TraceForBlock(start, end, true);
		}
	}
}

void ASelectionPawn::HandleHitCell(ACell* hit_cell)
{
	if (CurrentCellFocus != hit_cell)
	{
		if (CurrentCellFocus)
		{
			CurrentCellFocus->Highlight(false);
		}

		if (hit_cell)
		{
			hit_cell->Highlight(true);
		}

		CurrentCellFocus = hit_cell;
	}
}

void ASelectionPawn::HandleHitTower(ITower* hit_tower)
{
	ITower* curTower = nullptr;

	if (CurrentCellFocus)
	{
		curTower = Cast<ITower>(CurrentCellFocus->ChildTower);
	}

	if (curTower != hit_tower)
	{
		if (CurrentCellFocus)
		{
			CurrentCellFocus->Highlight(false);
		}

		if (hit_tower && hit_tower->OwningCell)
		{
			hit_tower->OwningCell->Highlight(true);
			CurrentCellFocus = hit_tower->OwningCell;
		}
	}
}

void ASelectionPawn::TraceForBlock(const FVector& Start, const FVector& End, const bool b_draw_debug_helpers)
{
	FHitResult hit_result;
	GetWorld()->LineTraceSingleByChannel(hit_result, Start, End, ECC_Visibility);

	if (b_draw_debug_helpers)
	{
		DrawDebugLine(GetWorld(), Start, hit_result.Location, FColor::Red);
		DrawDebugSolidBox(GetWorld(), hit_result.Location, FVector(20.0f), FColor::Red);
	}

	if (hit_result.Actor.IsValid())
	{
		const auto hit_cell = Cast<ACell>(hit_result.Actor.Get());
		if (hit_cell)
		{
			HandleHitCell(hit_cell);
		}

		const auto hit_tower = Cast<ITower>(hit_result.Actor.Get());
		if (hit_tower)
		{
			HandleHitTower(hit_tower);
		}
	}

	else if (CurrentCellFocus)
	{
		CurrentCellFocus->Highlight(false);
		CurrentCellFocus = nullptr;
	}
}

void ASelectionPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("TriggerClick", EInputEvent::IE_Pressed, this, &ASelectionPawn::TriggerClick);
}

void ASelectionPawn::TriggerClick()
{
	if (CurrentCellFocus)
	{
		CurrentCellFocus->HandleClicked();
	}
}

bool ASelectionPawn::GEngineAndViewportExists()
{
	return GEngine &&
		   GEngine->GameViewport &&
		   GEngine->GameViewport->Viewport;
}