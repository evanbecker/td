// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "SelectionPawn.generated.h"

class ITower;
class ACell;
UCLASS(config = Game)
class TD_API ASelectionPawn : public APawn
{
	GENERATED_UCLASS_BODY()

public:
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	virtual void BeginPlay() override;
	void CalculateCameraTransform(float mouse_x, float mouse_y, FVector2D viewport_size);
	static bool GEngineAndViewportExists();
	void TriggerClick();
	void HandleHitTower(ITower* hit_tower);
	void HandleHitCell(::ACell* hit_cell);
	void TraceForBlock(const FVector& Start, const FVector& End, const bool b_draw_debug_helpers);

	const float EdgeLeftRightSize = 60.f;
	const float EdgeTopBottomSize = 40.f;
	const float Speed = 10.f;

	UPROPERTY(EditAnywhere)
	USpringArmComponent* OurCameraSpringArm;
	UCameraComponent* OurCamera;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	class ACell* CurrentCellFocus;
};