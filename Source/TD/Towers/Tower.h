// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Grid/Cell.h"
#include "Tower.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTower : public UInterface
{
	GENERATED_BODY()
};

class TD_API ITower
{
	GENERATED_BODY()
public:
	class USceneComponent* DummyRoot;
	class ACell* OwningCell;
	float Cost;
	float Damage;
	float AttackSpeed;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Attack();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void HandleClicked();
};