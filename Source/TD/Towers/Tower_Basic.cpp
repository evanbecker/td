// Copyright 2018 DevNull LLC, All Rights Reserved.

#include "Tower_Basic.h"
#include "Materials/Material.h"
#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"

struct FConstructorStatics
{
	ConstructorHelpers::FObjectFinderOptional<UStaticMesh> TowerMesh;
	ConstructorHelpers::FObjectFinderOptional<UMaterial> MTower;
	FConstructorStatics():
		TowerMesh(TEXT("/Game/Assets/Towers/Models/Towers/Tower_Base1A.Tower_Base1A")),
		MTower(TEXT("/Game/Assets/Towers/materials/Towers/Tower_base1.Tower_base1"))
	{
	}
};
static FConstructorStatics constructor_statics;

void ATower_Basic::SetDummyRoot()
{
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;
}

ATower_Basic::ATower_Basic()
{
	PrimaryActorTick.bCanEverTick = true;
	Damage = 50;
	SetDummyRoot();
	SetTowerMesh();
	MTower = constructor_statics.MTower.Get();
}

void ATower_Basic::SetTowerMesh()
{
	TowerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TowerMesh0"));
	TowerMesh->SetStaticMesh(constructor_statics.TowerMesh.Get());
	TowerMesh->SetRelativeScale3D(FVector(1.f, 1.f, 1.f));
	TowerMesh->SetRelativeLocation(FVector(-100.f, -100.f, 0.f));
	TowerMesh->SetMaterial(0, constructor_statics.MTower.Get());
	TowerMesh->SetupAttachment(DummyRoot);
	TowerMesh->OnClicked.AddDynamic(this, &ATower_Basic::TowerClicked);
	TowerMesh->OnInputTouchBegin.AddDynamic(this, &ATower_Basic::OnFingerPressedTower);
}

void ATower_Basic::OnFingerPressedTower(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
	HandleClicked_Implementation();
}

void ATower_Basic::TowerClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	HandleClicked_Implementation();
}

void ATower_Basic::BeginPlay()
{
	Super::BeginPlay();
}

void ATower_Basic::Attack_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Attack Damage Test: %f!"), Damage);
}

void ATower_Basic::HandleClicked_Implementation()
{
	OwningCell->HandleClicked();
}

void ATower_Basic::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}