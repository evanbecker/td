// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "GameFramework/Actor.h"
#include "Tower_Basic.generated.h"

UCLASS()
class TD_API ATower_Basic : public AActor, public ITower
{
	GENERATED_BODY()

	UPROPERTY(Category = Tower, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* TowerMesh;
	
public:
	void SetTowerMesh();
	void SetDummyRoot();
	ATower_Basic();

	UPROPERTY()
	class UMaterial* MTower;

	UFUNCTION()
	void OnFingerPressedTower(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	void TowerClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked);

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void Attack_Implementation() override;

	virtual void HandleClicked_Implementation() override;
};