// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "UTower.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UUTower : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IUTower
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Upgrade")
	bool TowerUpgrade();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Attack")
	bool TowerAttack();
	
};
