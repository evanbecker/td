// Copyright 2018 DevNull LLC, All Rights Reserved.

#include "TDGameModeBase.h"
#include "Selection/SelectionPlayerController.h"
#include "Selection/SelectionPawn.h"

ATDGameModeBase::ATDGameModeBase()
{
	DefaultPawnClass = ASelectionPawn::StaticClass();
	PlayerControllerClass = ASelectionPlayerController::StaticClass();
}