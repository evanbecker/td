// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGameModeBase.generated.h"

UCLASS()
class ATDGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDGameModeBase();
};