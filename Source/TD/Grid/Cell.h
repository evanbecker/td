// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Selection/SelectionEnum.h"
#include "Cell.generated.h"

UCLASS()
class TD_API ACell : public AActor
{
	GENERATED_BODY()

	UPROPERTY(Category = Cell, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

	UPROPERTY(Category = Cell, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* CellMesh;

	void SpawnTower();

public:
	ACell();
	void SetDummyRoot();
	void SetCellMesh();
	void SetSelectionMaterials();
	bool BIsActive;
	bool BHasTower;

	UPROPERTY()
	class UMaterial* SelectionOff;

	UPROPERTY()
	class UMaterial* SelectionHighlight;

	UPROPERTY()
	class UMaterial* SelectionOn;

	UPROPERTY()
	class ACellGrid* OwningGrid;

	UPROPERTY()
	class AActor* ChildTower;

	UPROPERTY(Category = Cell, EditAnywhere, BlueprintReadOnly)
	float TowerHeight;

	UFUNCTION()
	void CellClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked);

	UFUNCTION()
	void UpdateMaterial();

	UFUNCTION()
	void UpdateMaterialEnum(SelectEnum e);

	UFUNCTION()
	void OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent);

	void HandleClicked();
	void Highlight(bool b_on);

protected:
	virtual void BeginPlay() override;
};