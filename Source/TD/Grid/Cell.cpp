// Copyright 2018 DevNull LLC, All Rights Reserved.

#include "Cell.h"
#include "CellGrid.h"
#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"
#include "Towers/Tower_Basic.h"

class UMaterialInstance;

ACell::ACell()
{
	PrimaryActorTick.bCanEverTick = true;
	BIsActive = false;
	SetDummyRoot();
	SetCellMesh();
	SetSelectionMaterials();
	TowerHeight = 10;
}

struct FConstructorStatics
{
	ConstructorHelpers::FObjectFinderOptional<UStaticMesh> Selection;
	ConstructorHelpers::FObjectFinderOptional<UMaterial> MSelection_On;
	ConstructorHelpers::FObjectFinderOptional<UMaterial> MSelection_Highlight;
	ConstructorHelpers::FObjectFinderOptional<UMaterial> MSelection_Off;
	FConstructorStatics():
		Selection(TEXT("/Game/selection.selection")),
		MSelection_On(TEXT("/Game/Assets/Selection/Materials/MSelection_On.MSelection_On")),
		MSelection_Highlight(TEXT("/Game/Assets/Selection/Materials/MSelection_Highlight.MSelection_Highlight")),
		MSelection_Off(TEXT("/Game/Assets/Selection/Materials/MSelection_Off.MSelection_Off"))
	{
	}
};
static FConstructorStatics constructor_statics;

void ACell::SetDummyRoot()
{
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;
}

void ACell::SetCellMesh()
{
	CellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CellMesh0"));
	CellMesh->SetStaticMesh(constructor_statics.Selection.Get());
	CellMesh->SetRelativeScale3D(FVector(230.f,230.f,10.f));
	CellMesh->SetRelativeLocation(FVector(0.f,0.f,0.f));
	CellMesh->SetMaterial(0, constructor_statics.MSelection_Off.Get());
	CellMesh->SetupAttachment(DummyRoot);
	CellMesh->OnClicked.AddDynamic(this, &ACell::CellClicked);
	CellMesh->OnInputTouchBegin.AddDynamic(this, &ACell::OnFingerPressedBlock);
}

void ACell::SetSelectionMaterials()
{
	SelectionOn = constructor_statics.MSelection_On.Get();
	SelectionHighlight = constructor_statics.MSelection_Highlight.Get();
	SelectionOff = constructor_statics.MSelection_Off.Get();
}

void ACell::SpawnTower()
{
	const auto cell_location = FVector(100.f, -15.f, TowerHeight) + GetActorLocation();
	const auto tower = GetWorld()->SpawnActor<ATower_Basic>(cell_location, FRotator(0, 0, 0));

	if (tower != nullptr)
	{
		tower->OwningCell = this;
	}
}

void ACell::BeginPlay()
{
	Super::BeginPlay();
	SpawnTower();
}

void ACell::CellClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	HandleClicked();
}

void ACell::UpdateMaterial()
{
	if (BIsActive)
	{
		CellMesh->SetMaterial(0, SelectionOn);
	}
	else
	{
		CellMesh->SetMaterial(0, SelectionOff);
	}
}

void ACell::UpdateMaterialEnum(const SelectEnum e)
{
	switch(e)
	{
		case SelectEnum::SelOn:
			CellMesh->SetMaterial(0, SelectionOn);
			break;
		case SelectEnum::SelOff:
			CellMesh->SetMaterial(0, SelectionOff);
			break;
		case SelectEnum::SelHighlight:
			CellMesh->SetMaterial(0, SelectionHighlight);
			break;
	}
}

void ACell::OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
	HandleClicked();
}

void ACell::HandleClicked()
{
	if (BIsActive) 
	{
		return;
	}

	BIsActive = true;
	CellMesh->SetMaterial(0, SelectionOn);

	if (OwningGrid != nullptr)
	{
		OwningGrid->Select(this);
	}
}

void ACell::Highlight(const bool b_on)
{
	if (BIsActive)
	{
		return;
	}

	if (b_on)
	{
		CellMesh->SetMaterial(0, SelectionHighlight);
	}
	else
	{
		CellMesh->SetMaterial(0, SelectionOff);
	}
}