// Copyright 2018 DevNull LLC, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CellGrid.generated.h"

class ACell;
UCLASS()
class TD_API ACellGrid : public AActor
{
	GENERATED_BODY()

	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;
	
public:	
	ACellGrid();

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 Length;

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 Width;

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	float CellSpacing;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	class ACell* CurrentCellSelection{};

protected:
	virtual void BeginPlay() override;

public:
	void Select(ACell* selection);
};