// Copyright 2018 DevNull LLC, All Rights Reserved.

#include "CellGrid.h"
#include "Cell.h"
#include "Engine/World.h"

ACellGrid::ACellGrid()
{
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	PrimaryActorTick.bCanEverTick = true;
	Length = 10;
	Width = 5;
	CellSpacing = 230.f;
}

void ACellGrid::BeginPlay()
{
	Super::BeginPlay();

	for (auto i=0; i<Length; i++)
	{
		const auto x_offset = i * CellSpacing;
		for(auto k=0; k<Width; k++)
		{
			const auto y_offset = k * CellSpacing;
			const auto cell_location = FVector(x_offset, y_offset, 0.f) + GetActorLocation();
			const auto new_cell = GetWorld()->SpawnActor<ACell>(cell_location, FRotator(0, 0, 0));
			
			if (new_cell != nullptr) 
			{
				new_cell->OwningGrid = this;
			}
		}
	}
}

void ACellGrid::Select(ACell* selection)
{
	if (CurrentCellSelection == nullptr)
	{
		CurrentCellSelection = selection;
		return;
	}

	CurrentCellSelection->BIsActive = false;
	CurrentCellSelection->UpdateMaterial();
	CurrentCellSelection = selection;
}