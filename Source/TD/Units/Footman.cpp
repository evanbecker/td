// Copyright 2018 DevNull LLC, All Rights Reserved.

#include "Footman.h"
#include "Components/ArrowComponent.h"

AFootman::AFootman()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFootman::BeginPlay()
{
	Super::BeginPlay();
}

void AFootman::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFootman::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}