# Tower Defense Project
A Tower Defense game built using Unreal Engine 4. These files may not be redistributed. All rights reserved.

## Getting Started
For a contributing guide, please see our [CONTRIBUTING.md](CONTRIBUTING.md) file. This file serves both as the guide for building and setup as well as contributing.

### Prerequisites
Unreal Engine 4

### Tools
Visual Studio or Ryder (Optional)
ReSharper (Optional)
Git

### Installing
Installer coming soon.
Hit compile in the editor. Afterwards, right click the .uproject file in explorer to bring open the windows context menu and select "Generate Visual Studio project files". The .sln and other files will be generated. For a more verbose and modern breakdown, see the [CONTRIBUTING.md](CONTRIBUTING.md) file.

### Running the tests
Tests coming soon.

#### Deployment
Deployment notes coming soon. This will also include internal CI/CD build processes, as well as ideally "containerizing" the project with Docker.

### Built With
[Unreal Engine 4](https://www.unrealengine.com/) - Game Engine.

## Authors
* **Evan Becker** - *Owner* - [GitHub](https://github.com/beckerej)

## License
[LICENSE.md](LICENSE.md) file coming soon.